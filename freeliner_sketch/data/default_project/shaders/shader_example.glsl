// example of a shader for use in freeliner shader layers

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

// incoming frame
uniform sampler2D texture;
// previous frame, used for feedback effects
uniform sampler2D ppixels;
uniform vec2 texOffset;
varying vec4 vertColor;
varying vec4 vertTexCoord;

// uniforms controllable via webui sliders and OSC
// command example :
// layer layerName uniforms (0-7) 0.42
// as OSC /layer/firstShader/uniforms/2/0.42

uniform float u1;
uniform float u2;
uniform float u3;
uniform float u4;
uniform float u5;
uniform float u6;
uniform float u7;
uniform float u8;

// float smoothing values set between 1.0 and 20.0 or more
// this gets parsed by freeliner, it will dampen the input
// 1.0 = no damping, 10.0 = pretty smooth
//*** damp u1 10.0
//*** damp u2 1.0
//*** damp u3 10.0
//*** damp u4 10.0


// time is connected to freeliner time/speed
uniform float time;
// texture resolution in pixels
uniform vec2 res;

void main(void) {
    // copy position
    vec2 pos = vertTexCoord.xy;
    // fetch color of pixel
    vec4 col = texture2D(texture, vertTexCoord.xy);
    // make a float value for our effect
    // u2 controls the speed of the effect
    float f = time*.6*(u2+0.1);
    // do some weird stuff
    // u3 and u4 control parameters of the effect
    f += sin(sin(u3*pos.y*10.+f)+pos.x*20.*u4);
    f = fract(f);
    // make a modified color
    vec4 otherCol = col*vec4(f);
    // u1 is used to mix in the effect
    gl_FragColor = mix(col, otherCol, vec4(u1+0.03));
}

// if you have no idea what is going on in here have a look at :
// https://thebookofshaders.com/
